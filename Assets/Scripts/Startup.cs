using Gamebase;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using Leopotam.EcsLite.ExtendedSystems;
using UnityEngine;
using UnityEngine.Serialization;
using Voody.UniLeo.Lite;
using Zenject;

public class Startup : MonoBehaviour
{
    private EcsWorld ecsWorld;
    private EcsSystems updateSystems;
    private EcsSystems fixedUpdateSystems;

    [SerializeField] private PlayerConfig _playerConfig;
    [FormerlySerializedAs("_levelConfig")] [SerializeField] private LevelSpawnersConfig _levelSpawnersConfig;
    [SerializeField] private WeaponConfig _weaponConfig;
    [SerializeField] private ExperienceConfig _experienceConfig;
    [SerializeField] private UiComponentPrefabConfig _uiComponentPrefabs;
    [SerializeField] private GlobalWeaponConfig _globalWeaponSettings;
    [SerializeField] private GameSettings _gameSettings;
    [SerializeField] private ObjectsParents _objectsParents;
    private ProgressSystem _progressSystem;
    private GlobalEventsSystem _globalEventsSystem;

    [Inject]
    private void Construct(ProgressSystem progressSystem, GlobalEventsSystem globalEventsSystem)
    {
        _progressSystem = progressSystem;
        _globalEventsSystem = globalEventsSystem;
    }
    private void Awake()
    {
        _progressSystem.ResetProgress();
        ecsWorld = new EcsWorld();

        updateSystems = new EcsSystems(ecsWorld)
            .ConvertScene()
            .Add(new InitParentComponentsSystem()) //
            .Add(new SpawnPlayerSystem()) //
            .Add(new PlayerInitSystem()) //
            .Add(new PlayerInputSystem()) //
            .Add(new CameraInitSystem()) //
            .Add(new TimerSystem()) //
            .Add(new TimerUiSystem()) //
            .Add(new UpdateExperienceUiSystem()) //
            // Системы спавна врагов
            .Add(new EnemySpawnerCreateSystem()) //
            .Add(new EnemySpawnerDestroySystem()) //
            .Add(new EnemySpawnerSystem()) //
            .Add(new SelectEnemySpawnPointSystem()) //
            .Add(new CreateEnemySystem()) //
            .Add(new EnemyInitSystem()) //
            
            .Add(new CreateHealthbarSystem()) //
            // Улучшение оружий
            .Add(new BatWeaponUpgradeSystem())
            .Add(new CommonWeaponUpgradeSystem())
            .Add(new WaterWeaponUpgradeSystem())
            .Add(new ContactWeaponUpgradeSystem())
            
            .Add(new AddWeaponIconSystem()) //
            .Add(new SplitContactsSystem()) // переделать
            .DelHere<NearestEnemyComponent>()
            .Add(new FindNearestEnemySystem()) //
            // Атака оружий
            .Add(new CommonWeaponAttackSystem())
            .Add(new WaterWeaponFirstAttackSystem())
            .Add(new WaterWeaponSecondAttackSystem())
            .Add(new ContactWeaponAttackSystem())
            .Add(new BatWeaponAttackSystem())
            
            .Add(new ProjectileHitSystem()) // --
            .Add(new CollectCrystalsSystem()) //
            .Add(new DestroyProjectileSystem()) //
            .DelHere<InteractionComponent>()
            .Add(new LevelUpSystem())
            .Add(new InitLevelUpPanelSystem())
            .Add(new HealthbarSystem()) //
            .Add(new CheckWinGameSystem()) //
            .Add(new EnemyKillSystem()) //
            .Add(new PlayerKillSystem()) //
            .Add(new KillsCounterUiSystem()) //
            .Add(new DropCrystalSystem()) //
            .Add(new EndGameUiSystem()) //
            .Inject(_levelSpawnersConfig, _weaponConfig, _uiComponentPrefabs, _experienceConfig, _progressSystem,
                _globalEventsSystem, _globalWeaponSettings, _playerConfig, _gameSettings, _objectsParents);
        updateSystems.Init();
        
        fixedUpdateSystems = new EcsSystems(ecsWorld)
            .Add(new DirectionMoveSystem()) //
            .Add(new RigidbodyFollowingPlayerSystem()) // 
            .Add(new TransformFollowingPlayerSystem()) //
            .Add(new MoveAroundSystem()) //
            .DelHere<InteractionComponent>()
            .Add(new DestroyOutsideCameraSystem()) //
            .Inject();
        fixedUpdateSystems.Init();
    }
    // Перенести проверку коллизий в Fixed
    private void Update()
    {
        updateSystems.Run();
    }

    private void FixedUpdate()
    {
        fixedUpdateSystems.Run();
    }

    private void OnDestroy()
    {
        updateSystems.Destroy();
        fixedUpdateSystems.Destroy();
        ecsWorld.Destroy();
    }

}
