﻿using System.Collections.Generic;
using Voody.UniLeo.Lite;

public class EntityWeaponsProvider : MonoProvider<EntityWeaponsComponent>
{
    protected override void Initialize()
    {
        base.Initialize();
        value.Weapons = new Dictionary<WeaponType, WeaponLevel>();
    }
}