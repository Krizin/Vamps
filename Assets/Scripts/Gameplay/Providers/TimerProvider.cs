﻿using UnityEngine;
using Voody.UniLeo.Lite;

public class TimerProvider : MonoProvider<TimerComponent>
{
    protected override void Initialize()
    {
        base.Initialize();
        value.StartTime = Time.time;
    }
}