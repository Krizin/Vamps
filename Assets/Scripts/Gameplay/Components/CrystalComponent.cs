﻿using UnityEngine;

public struct CrystalComponent
{
    public Vector3 Position;
    public int Experience;
}