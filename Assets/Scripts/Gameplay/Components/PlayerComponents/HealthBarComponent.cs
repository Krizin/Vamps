﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct HealthBarComponent
{
    [HideInInspector] public Slider Slider;
    public Vector3 Offset;
}