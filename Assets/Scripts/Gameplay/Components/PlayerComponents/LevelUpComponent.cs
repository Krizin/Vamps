﻿using System.Collections.Generic;

public struct LevelUpComponent
{
    public List<WeaponType> Upgrades;
}