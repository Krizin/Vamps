﻿using UnityEngine;

public struct TrySpawnEnemyComponent
{
    public EnemyConfig EnemyConfig;
    public Vector3 Spawnpoint;
    public float Spread;
    public int Count;
}