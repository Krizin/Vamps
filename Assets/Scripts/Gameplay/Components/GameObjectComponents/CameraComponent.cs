﻿using UnityEngine;

public struct CameraComponent
{
    public Camera Camera;
    public Vector3 curVelocity;
    public Vector3 offset;
    public float cameraSmoothness;
}