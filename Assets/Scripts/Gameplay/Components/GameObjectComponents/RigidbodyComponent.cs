﻿using System;
using UnityEngine;

[Serializable]
public struct RigidbodyComponent
{
    public Rigidbody Rigidbody;
}