﻿public struct ProjectileComponent
{
    public int Damage;
    public int HitCount;
    public int HitLimit;
    public float Lifetime;
    public float LifetimeLimit;
    public float LastHitTime;
}