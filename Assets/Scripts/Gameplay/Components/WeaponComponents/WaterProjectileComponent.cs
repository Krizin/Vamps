﻿using UnityEngine;

public struct WaterProjectileComponent
{
    public int Damage;
    public float SizeMultiplier;
    public float LifetimeLimit;
    public int HitLimit;

    public GameObject SecondProjectilePrefab;
}