﻿using UnityEngine;

public struct CommonWeaponComponent : IWeaponComponent
{
    public int Damage;
    public float Reload;
    public float LastShotTime;
    public float Speed;
    public float TriggerRadius;
    public float ProjectileLifetimeLimit;
    public int ProjectileHitLimit;
    public GameObject ProjectilePrefab;
}