﻿using Leopotam.EcsLite;

public struct UpgradeWeaponComponent
{
    public EcsPackedEntity Entity;
    public WeaponType WeaponType;
}