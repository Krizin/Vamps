﻿using System.Collections.Generic;

public struct EntityWeaponsComponent
{
    public Dictionary<WeaponType, WeaponLevel> Weapons;
}

public struct WeaponLevel
{
    public int Level;
    public int MaxLevel;
}