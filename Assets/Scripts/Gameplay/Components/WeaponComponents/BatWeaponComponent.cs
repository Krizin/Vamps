﻿using UnityEngine;

public struct BatWeaponComponent : IWeaponComponent
{
    public int Damage;
    public float Reload;
    public float LastShotTime;
    public int ProjectilesCount;
    public float Radius;
    public float Speed;
    public float ProjectileLifetimeLimit;
    public int ProjectileHitLimit;
    public GameObject ProjectilePrefab;
}