﻿using UnityEngine;

public struct WaterWeaponComponent : IWeaponComponent
{
    public int Damage;
    public int Count;
    public float SizeMultiplier;
    public float ProjectileFlightTime;
    public float Reload;
    public float LastShotTime;
    public float ProjectileLifetimeLimit;
    public int HitLimit;
    public GameObject FirstProjectilePrefab;
    public GameObject SecondProjectilePrefab;
}