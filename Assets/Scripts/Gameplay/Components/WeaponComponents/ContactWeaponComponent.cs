﻿public struct ContactWeaponComponent : IWeaponComponent
{
    public int Damage;
    public float LastHitTime;
}