﻿using UnityEngine;

public struct DirectionMoveComponent
{
    public float Speed;
    public Vector3 Direction;
}