﻿using UnityEngine;

public struct RigidbodyFollowingPlayerComponent
{
    public Vector3 Offset;
    public float Speed;
}