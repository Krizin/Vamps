﻿using UnityEngine;

public struct SmoothFollowingPlayerComponent
{
    public Vector3 Offset;
    public Vector3 Velocity;
    public float Smoothness;
}