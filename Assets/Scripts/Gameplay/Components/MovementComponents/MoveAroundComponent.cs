﻿using UnityEngine;

public struct MoveAroundComponent
{
    public Transform Target;
    public Vector3 CurrentOffset;
    public float Speed;
}