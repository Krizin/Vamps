﻿using Leopotam.EcsLite;
using UnityEngine;

public struct InteractionComponent : IEcsAutoReset<InteractionComponent>
{
    public Transform Owner;
    public EcsPackedEntity OwnerEntity;
    public EntityType OwnerType;
    public Transform Other;
    public EcsPackedEntity OtherEntity;
    public EntityType OtherType;
    public void AutoReset(ref InteractionComponent c)
    {
        c.OwnerType = EntityType.Unidentified;
        c.OtherType = EntityType.Unidentified;
    }
}