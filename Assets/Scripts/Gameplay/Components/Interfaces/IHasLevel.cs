﻿public interface IHasLevel
{
    public int Level { get; }
    public int MaxLevel { get; }
}