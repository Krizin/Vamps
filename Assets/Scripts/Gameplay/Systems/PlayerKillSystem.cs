﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Обработка смерти игрока
/// </summary>
public class PlayerKillSystem : IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsFilterInject<Inc<PlayerComponent, HealthComponent, TransformComponent>> _playerFilter = default;
    private readonly EcsFilterInject<Inc<EndGameComponent>> _endGameFilter = default;
    private readonly EcsPoolInject<HealthComponent> _healthPool = default;
    private readonly EcsPoolInject<EndGameComponent> _endGamePool = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var playerEntity in _playerFilter.Value)
        {
            ref var health = ref _healthPool.Value.Get(playerEntity);
            if (health.CurrentHealth > 0) continue;
            
            ref var transformComponent = ref _transformPool.Value.Get(playerEntity);
            if (_endGameFilter.Value.GetEntitiesCount() == 0)
            {
                var endGameEntity = _world.Value.NewEntity();
                ref var endGameComponent = ref _endGamePool.Value.Add(endGameEntity);
                endGameComponent.GameResult = GameResult.Lose;
            }

            transformComponent.Transform.gameObject.SetActive(false);

            return;
        }
    }

}