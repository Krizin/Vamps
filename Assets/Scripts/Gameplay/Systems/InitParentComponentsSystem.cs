﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Создание родительских компонентов/объектов
/// </summary>
public class InitParentComponentsSystem : IEcsInitSystem
{
    private EcsWorldInject _world = default;
    private EcsPoolInject<TransformComponent> _transformPool = default;
    private EcsPoolInject<EnemyParentComponent> _enemyParentPool = default;
    private EcsPoolInject<ProjectileParentComponent> _projectileParentPool = default;
    private EcsPoolInject<CrystalParentComponent> _crystalParentPool = default;
    public void Init(EcsSystems systems)
    {
        var enemyParentEntity = _world.Value.NewEntity();
        var enemyParent = new GameObject("Enemies");
        _enemyParentPool.Value.Add(enemyParentEntity);
        _transformPool.Value.Add(enemyParentEntity).Transform = enemyParent.transform;

        var projectileParentEntity = _world.Value.NewEntity();
        var projectileParent = new GameObject("Projectiles");
        _projectileParentPool.Value.Add(projectileParentEntity);
        _transformPool.Value.Add(projectileParentEntity).Transform = projectileParent.transform;
        
        var crystalParentEntity = _world.Value.NewEntity();
        var crystalParent = new GameObject("Crystals");
        _crystalParentPool.Value.Add(crystalParentEntity);
        _transformPool.Value.Add(crystalParentEntity).Transform = crystalParent.transform;
    }
}