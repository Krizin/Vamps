﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Определяет ближайшего в игроку врага
/// </summary>
public class FindNearestEnemySystem : IEcsRunSystem
{
    private EcsPackedEntity _playerEntity;
    private EcsFilterInject<Inc<PlayerComponent, TransformComponent>> _playerFilter = default;
    private EcsFilterInject<Inc<EnemyComponent, TransformComponent>> _enemiesFilter = default;
    private EcsPoolInject<TransformComponent> _transformPool = default;
    private EcsPoolInject<NearestEnemyComponent> _nearestEnemyPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var playerEntity in _playerFilter.Value)
        {
            var playerPosition = _transformPool.Value.Get(playerEntity).Transform.position;

            var nearestEnemyDirection = Vector3.one * float.MaxValue;
            var nearestEnemyEntity = -1;
            foreach (var enemyEntity in _enemiesFilter.Value)
            {
                ref var enemyTransform = ref _transformPool.Value.Get(enemyEntity);
                if (!((enemyTransform.Transform.position - playerPosition).magnitude <
                      nearestEnemyDirection.magnitude)) continue;
                nearestEnemyDirection = enemyTransform.Transform.position - playerPosition;
                nearestEnemyEntity = enemyEntity;
            }

            if (nearestEnemyEntity != -1)
                _nearestEnemyPool.Value.Add(nearestEnemyEntity);
        }
    }
}