﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Создает спавнеры врагов по таймеру
/// </summary>
public class EnemySpawnerCreateSystem : IEcsRunSystem
{
    private EcsWorldInject _world = default;
    private readonly EcsCustomInject<LevelSpawnersConfig> _levelConfig;
    private EcsFilterInject<Inc<GameTimerComponent, TimerComponent>> _timerFilter = default;
    private EcsPoolInject<EnemySpawnerComponent> _enemySpawnerPool = default;

    public void Run(EcsSystems systems)
    {
        foreach (var timerEntity in _timerFilter.Value)
        {
            ref var timerComponent = ref _timerFilter.Pools.Inc2.Get(timerEntity);
            
            var lastUpdateTime = timerComponent.PassedTime - Time.deltaTime;
            
            foreach (var enemySpawnData in _levelConfig.Value.EnemySpawners)
            {
                if (!(enemySpawnData.StartSpawnTime > lastUpdateTime) ||
                    !(enemySpawnData.StartSpawnTime < timerComponent.PassedTime)) continue;
                
                var enemySpawnerEntity = _world.Value.NewEntity();
                _enemySpawnerPool.Value.Add(enemySpawnerEntity);
                ref var enemySpawnerComponent = ref _enemySpawnerPool.Value.Get(enemySpawnerEntity);
                enemySpawnerComponent.SpawnerNumber = _levelConfig.Value.EnemySpawners.IndexOf(enemySpawnData);
            }
        }
    }

}