﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Определяет позицию спавна для врагов
/// </summary>
public class SelectEnemySpawnPointSystem : IEcsRunSystem
{
    private EcsCustomInject<GameSettings> _gameSettings;
    private EcsFilterInject<Inc<PlayerComponent, TransformComponent>> _playerFilter = default;
    private EcsFilterInject<Inc<TrySpawnEnemyComponent>> _trySpawnEnemyFilter = default;
    private EcsPoolInject<TransformComponent> _transformPool = default;
    private EcsPoolInject<TrySpawnEnemyComponent> _trySpawnEnemyPool = default;
    public void Run(EcsSystems systems)
    {
        Transform playerTransform = null;
        foreach (var entity in _playerFilter.Value)
        {
            playerTransform = _transformPool.Value.Get(entity).Transform;
            break;
        }
        if (playerTransform == null) return;
        
        foreach (var entity in _trySpawnEnemyFilter.Value)
        {
            ref var trySpawnEnemyComponent = ref _trySpawnEnemyPool.Value.Get(entity);
            Vector3 spawnPoint = Random.insideUnitCircle.normalized * _gameSettings.Value.SpawnRadius;
            spawnPoint.z = spawnPoint.y;
            spawnPoint.y = 0;
            spawnPoint += playerTransform.position;
            trySpawnEnemyComponent.Spawnpoint = spawnPoint;
        }
    }
}