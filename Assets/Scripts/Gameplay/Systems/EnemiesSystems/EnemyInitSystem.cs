﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

public class EnemyInitSystem : IEcsRunSystem
{
    private EcsWorldInject _world = default;
    private EcsFilterInject<Inc<EnemyInitComponent>> _enemyFilter = default;
    private EcsPoolInject<EnemyComponent> _enemyPool = default;
    private EcsPoolInject<RigidbodyFollowingPlayerComponent> _followingObjectPool = default;
    private EcsPoolInject<HealthComponent> _healthPool = default;
    private EcsPoolInject<UpgradeWeaponComponent> _upgradeWeaponPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _enemyFilter.Value)
        {
            var enemyConfig = _enemyFilter.Pools.Inc1.Get(entity).EnemyConfig;
            
            ref var enemy = ref _enemyPool.Value.Get(entity);
            enemy.Experience = enemyConfig.Experience;

            ref var followingPlayer = ref _followingObjectPool.Value.Get(entity);
            followingPlayer.Speed = enemyConfig.Speed;

            ref var health = ref _healthPool.Value.Get(entity);
            health.CurrentHealth = health.MaxHealth = enemyConfig.Health;
            
            foreach (var weaponType in enemyConfig.BaseWeapons)
            {
                var upgradeWeaponEntity = _world.Value.NewEntity();
                ref var upgradeWeaponComponent = ref _upgradeWeaponPool.Value.Add(upgradeWeaponEntity);
                upgradeWeaponComponent.Entity = _world.Value.PackEntity(entity);
                upgradeWeaponComponent.WeaponType = weaponType;
            }
            _enemyFilter.Pools.Inc1.Del(entity);
        }
    }
}