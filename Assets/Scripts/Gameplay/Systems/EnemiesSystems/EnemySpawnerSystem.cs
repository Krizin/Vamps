﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Создает компоненты-метки, по которым будут создаваться враги
/// </summary>
public class EnemySpawnerSystem : IEcsRunSystem
{
    private EcsWorldInject _world = default;
    private readonly EcsCustomInject<LevelSpawnersConfig> _levelConfig;
    private EcsFilterInject<Inc<EnemySpawnerComponent>> _enemySpawnerFilter = default;
    private EcsPoolInject<EnemySpawnerComponent> _enemySpawnerPool = default;
    private EcsPoolInject<TrySpawnEnemyComponent> _trySpawnEnemyPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _enemySpawnerFilter.Value)
        {
            ref var enemySpawnerComponent = ref _enemySpawnerPool.Value.Get(entity);
            var spawnerConfig = _levelConfig.Value.EnemySpawners[enemySpawnerComponent.SpawnerNumber];
            if (enemySpawnerComponent.LastSpawnTime != 0 && enemySpawnerComponent.LastSpawnTime + spawnerConfig.SpawnRate > Time.time) continue;
            
            enemySpawnerComponent.LastSpawnTime = Time.time;
            enemySpawnerComponent.CurrentTriggersCount++;
            
            var trySpawnEnemy = _world.Value.NewEntity();
            ref var trySpawnComponent = ref _trySpawnEnemyPool.Value.Add(trySpawnEnemy);
            trySpawnComponent.EnemyConfig = spawnerConfig.EnemyConfig;
            trySpawnComponent.Count = spawnerConfig.Count;
            trySpawnComponent.Spread = spawnerConfig.Spread;
        }
        
    }
}