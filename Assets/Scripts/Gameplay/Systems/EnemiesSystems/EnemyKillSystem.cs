﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Обработка смерти врагов
/// </summary>
public class EnemyKillSystem : IEcsInitSystem, IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsFilterInject<Inc<EnemyComponent, HealthComponent, TransformComponent>> _targetFilter = default;
    private readonly EcsFilterInject<Inc<KillsCounterComponent>> _killsCounterFilter = default;
    private readonly EcsPoolInject<EnemyComponent> _enemyPool = default;
    private readonly EcsPoolInject<HealthComponent> _healthPool = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    private readonly EcsPoolInject<CrystalComponent> _crystalPool = default;
    private readonly EcsPoolInject<KillsCounterComponent> _killsCounterPool = default;
    public void Init(EcsSystems systems)
    {
        _killsCounterPool.Value.Add(_world.Value.NewEntity());
    }
    public void Run(EcsSystems systems)
    {
        foreach (var enemyEntity in _targetFilter.Value)
        {
            ref var health = ref _healthPool.Value.Get(enemyEntity);
            if (health.CurrentHealth > 0) continue;
            
            ref var transformComponent = ref _transformPool.Value.Get(enemyEntity);
            // TODO: Реализовать через пул
            Object.Destroy(transformComponent.Transform.gameObject);
            ref var enemyComponent = ref _enemyPool.Value.Get(enemyEntity);
            var crystalEntity = _world.Value.NewEntity();
            ref var crystalComponent = ref _crystalPool.Value.Add(crystalEntity);
            crystalComponent.Position = transformComponent.Transform.position;
            crystalComponent.Experience = enemyComponent.Experience;
            
            foreach (var killsCounterEntity in _killsCounterFilter.Value)
            {
                _killsCounterPool.Value.Get(killsCounterEntity).Kills++;
            }
            _world.Value.DelEntity(enemyEntity);
        }
    }
}