﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Уничтажает спавнеры врагов по времени или по количеству срабатываний
/// </summary>
public class EnemySpawnerDestroySystem : IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsCustomInject<LevelSpawnersConfig> _levelConfig;
    
    private readonly EcsFilterInject<Inc<GameTimerComponent, TimerComponent>> _timerFilter = default;
    private readonly EcsFilterInject<Inc<EnemySpawnerComponent>> _enemySpawnerFilter = default;
    
    private readonly EcsPoolInject<TimerComponent> _timerPool = default;
    private readonly EcsPoolInject<EnemySpawnerComponent> _enemySpawnerPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var timerEntity in _timerFilter.Value)
        {
            ref var timerComponent = ref _timerPool.Value.Get(timerEntity);
            foreach (var enemySpawnerEntity in _enemySpawnerFilter.Value)
            {
                ref var enemySpawnerComponent = ref _enemySpawnerPool.Value.Get(enemySpawnerEntity);
                var spawnerConfig = _levelConfig.Value.EnemySpawners[enemySpawnerComponent.SpawnerNumber];
                switch (spawnerConfig.LifetimeLimitationType)
                {
                    case SpawnerLifetimeLimitationType.ByEndTime when spawnerConfig.EndSpawnTime < timerComponent.PassedTime:
                    case SpawnerLifetimeLimitationType.ByTriggersNumber when enemySpawnerComponent.CurrentTriggersCount >= spawnerConfig.TriggersNumber:
                        _world.Value.DelEntity(enemySpawnerEntity);
                        break;
                }
            }
        }
    }
}