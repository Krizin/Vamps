﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Создает врага
/// </summary>
public class CreateEnemySystem : IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private EcsCustomInject<ObjectsParents> _objectsParents;
    private readonly EcsFilterInject<Inc<TrySpawnEnemyComponent>> _trySpawnEnemyFilter = default;
    private readonly EcsPoolInject<TrySpawnEnemyComponent> _trySpawnEnemyPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _trySpawnEnemyFilter.Value)
        {
            ref var trySpawnEnemyComponent = ref _trySpawnEnemyPool.Value.Get(entity);
            var enemyConfig = trySpawnEnemyComponent.EnemyConfig;

            for (var i = 0; i < trySpawnEnemyComponent.Count; i++) {
                var instance = Object.Instantiate(enemyConfig.Prefab,
                    _objectsParents.Value.EnemiesParent);
                var offset = Random.insideUnitCircle * trySpawnEnemyComponent.Spread;
                instance.transform.position = trySpawnEnemyComponent.Spawnpoint + new Vector3(offset.x, 0, offset.y);
            }
            _world.Value.DelEntity(entity);
        }
    }
}