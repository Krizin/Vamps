﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Следование камеры за игроком
/// </summary>

public class CameraInitSystem : IEcsInitSystem
{
    private readonly EcsCustomInject<GameSettings> _gameSettings;
    private readonly EcsFilterInject<Inc<CameraComponent>> _cameraFilter = default;
    private readonly EcsPoolInject<CameraComponent> _cameraPool = default;
    private readonly EcsPoolInject<SmoothFollowingPlayerComponent> _transformFollowingObjectPool = default;
    public void Init(EcsSystems systems)
    {
        foreach (var entity in _cameraFilter.Value)
        {
            ref var camera = ref _cameraPool.Value.Get(entity);
            camera.Camera = Camera.main;

            ref var followingObjectComponent = ref _transformFollowingObjectPool.Value.Get(entity);
            followingObjectComponent.Offset = _gameSettings.Value.CameraOffset;
            followingObjectComponent.Velocity = Vector3.zero;
            followingObjectComponent.Smoothness = _gameSettings.Value.CameraSmoothness;
        }
    }
}