﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

public class RigidbodyFollowingPlayerSystem : IEcsRunSystem
{
    private readonly EcsFilterInject<Inc<PlayerComponent, TransformComponent>> _playerFilter = default;
    private readonly EcsFilterInject<Inc<RigidbodyFollowingPlayerComponent, TransformComponent, RigidbodyComponent>>
        _followingPlayerFilter = default;
    
    private readonly EcsPoolInject<RigidbodyComponent> _rigidbodyPool = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    private readonly EcsPoolInject<RigidbodyFollowingPlayerComponent> _followingObjectPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var playerEntity in _playerFilter.Value)
        {
            foreach (var entity in _followingPlayerFilter.Value)
            {
                ref var followingObject = ref _followingObjectPool.Value.Get(entity);
                
                var ownerPosition = _transformPool.Value.Get(entity).Transform.position;
                var targetPosition = _transformPool.Value.Get(playerEntity).Transform.position;
                var direction = (targetPosition + followingObject.Offset - ownerPosition).normalized;
                
                ref var ownerRigidbody = ref _rigidbodyPool.Value.Get(entity).Rigidbody;
                ownerRigidbody.velocity = followingObject.Speed * direction;
            }
        }
    }
}