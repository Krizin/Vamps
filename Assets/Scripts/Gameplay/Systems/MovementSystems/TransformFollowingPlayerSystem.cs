﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

public class TransformFollowingPlayerSystem : IEcsRunSystem
{
    private readonly EcsFilterInject<Inc<PlayerComponent, TransformComponent>> _playerFilter = default;
    private readonly EcsFilterInject<Inc<SmoothFollowingPlayerComponent, TransformComponent>> _followingPlayerFilter = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    private readonly EcsPoolInject<SmoothFollowingPlayerComponent> _transformFollowingPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var playerEntity in _playerFilter.Value)
        {
            ref var playerTransform = ref _transformPool.Value.Get(playerEntity);
            foreach (var followingEntity in _followingPlayerFilter.Value)
            {
                ref var followerTransform = ref _transformPool.Value.Get(followingEntity);
                ref var followingComponent = ref _transformFollowingPool.Value.Get(followingEntity);
                
                var followingPosition = followerTransform.Transform.position;
                var playerPosition = playerTransform.Transform.position;
                followerTransform.Transform.position = Vector3.SmoothDamp(followingPosition, playerPosition + followingComponent.Offset,
                    ref followingComponent.Velocity, followingComponent.Smoothness);
            }
        }
    }
}