﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Перемещение в определенном направлении
/// </summary>
public class DirectionMoveSystem : IEcsRunSystem
{
    private EcsFilterInject<Inc<DirectionMoveComponent, RigidbodyComponent>> _rigidbodyFilter = default;
    private EcsPoolInject<DirectionMoveComponent> _directionMovePool = default;
    private EcsPoolInject<RigidbodyComponent> _rigidbodyPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _rigidbodyFilter.Value)
        {
            ref var directionMoveComponent = ref _directionMovePool.Value.Get(entity);
            _rigidbodyPool.Value.Get(entity).Rigidbody.velocity =
                directionMoveComponent.Speed  * directionMoveComponent.Direction;
        }
    }
}