﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Движение вокруг заданного объекта
/// </summary>
public class MoveAroundSystem : IEcsRunSystem
{
    private readonly EcsFilterInject<Inc<MoveAroundComponent, TransformComponent>> _moveAroundFilter = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    private readonly EcsPoolInject<MoveAroundComponent> _moveAroundPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _moveAroundFilter.Value)
        {
            ref var transform = ref _transformPool.Value.Get(entity);
            ref var moveAround = ref _moveAroundPool.Value.Get(entity);
            moveAround.CurrentOffset = Quaternion.Euler(0, moveAround.Speed, 0) * moveAround.CurrentOffset;
            transform.Transform.position = moveAround.Target.position + moveAround.CurrentOffset;
        }
    }
}