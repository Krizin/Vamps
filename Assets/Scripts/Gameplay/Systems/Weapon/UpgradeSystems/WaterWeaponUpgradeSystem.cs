﻿public class WaterWeaponUpgradeSystem : UpgradeWeaponSystem<WaterWeaponComponent, WaterWeaponConfig>
{
    protected override void LevelUp(ref WaterWeaponComponent commonWeapon, WaterWeaponConfig config, int weaponLevel)
    {
        commonWeapon.Damage += config.WaterWeaponLevelUps[weaponLevel].Damage;
        commonWeapon.Reload += config.WaterWeaponLevelUps[weaponLevel].Reload;
        commonWeapon.ProjectileLifetimeLimit += config.WaterWeaponLevelUps[weaponLevel].ProjectileLifetimeLimit;
        commonWeapon.SizeMultiplier += config.WaterWeaponLevelUps[weaponLevel].BaseSizeMultilpier;
        commonWeapon.Count += config.WaterWeaponLevelUps[weaponLevel].Count;
    }

    protected override void Initialize(ref WaterWeaponComponent commonWeapon, WaterWeaponConfig config)
    {
        commonWeapon.Damage = config.BaseDamage;
        commonWeapon.SizeMultiplier = config.BaseSizeMultilpyer;
        commonWeapon.Count = config.Count;
        commonWeapon.ProjectileFlightTime = config.ProjectileFlightTime;
        commonWeapon.Reload = config.BaseReload;
        commonWeapon.FirstProjectilePrefab = config.FirstProjectilePrefab;
        commonWeapon.SecondProjectilePrefab = config.SecondProjectilePrefab;
        commonWeapon.ProjectileLifetimeLimit = config.ProjectileLifetimeLimit;
        commonWeapon.HitLimit = config.HitLimit;
    }
}