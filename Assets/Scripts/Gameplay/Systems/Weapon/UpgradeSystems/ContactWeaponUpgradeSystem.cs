﻿public class ContactWeaponUpgradeSystem : UpgradeWeaponSystem<ContactWeaponComponent, ContactWeaponConfig>
{
    protected override void LevelUp(ref ContactWeaponComponent commonWeapon, ContactWeaponConfig config, int weaponLevel)
    {
        commonWeapon.Damage += config.ContactLevelUps[weaponLevel].Damage;
    }

    protected override void Initialize(ref ContactWeaponComponent commonWeapon, ContactWeaponConfig config)
    {
        commonWeapon.Damage = config.BaseDamage;
    }
}