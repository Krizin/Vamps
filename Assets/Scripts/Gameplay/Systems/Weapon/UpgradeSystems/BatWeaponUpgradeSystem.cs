﻿public class BatWeaponUpgradeSystem : UpgradeWeaponSystem<BatWeaponComponent, BatWeaponConfig>
{
    protected override void LevelUp(ref BatWeaponComponent commonWeapon, BatWeaponConfig config, int weaponLevel)
    {
        commonWeapon.Damage += config.BatWeaponLevelUps[weaponLevel].Damage;
        commonWeapon.Reload += config.BatWeaponLevelUps[weaponLevel].Reload;
        commonWeapon.Radius += config.BatWeaponLevelUps[weaponLevel].Radius;
        commonWeapon.ProjectilesCount += config.BatWeaponLevelUps[weaponLevel].ProjectilesCount;
        commonWeapon.ProjectileLifetimeLimit += config.BatWeaponLevelUps[weaponLevel].ProjectileLifetimeLimit;
        commonWeapon.Speed += config.BatWeaponLevelUps[weaponLevel].Speed;
    }

    protected override void Initialize(ref BatWeaponComponent commonWeapon, BatWeaponConfig config)
    {
        commonWeapon.Damage = config.BaseDamage;
        commonWeapon.Speed = config.BaseSpeed;
        commonWeapon.Reload = config.BaseReload;
        commonWeapon.ProjectilePrefab = config.ProjectilePrefab;
        commonWeapon.ProjectileHitLimit = config.ProjectileHitLimit;
        commonWeapon.ProjectileLifetimeLimit = config.ProjectileLifetimeLimit;
        commonWeapon.ProjectilesCount = config.ProjectilesCount;
        commonWeapon.Radius = config.Radius;
    }
}