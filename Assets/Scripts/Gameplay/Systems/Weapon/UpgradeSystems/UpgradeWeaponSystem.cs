﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Добавление или улучшение оружия
/// </summary>
public abstract class UpgradeWeaponSystem<T1, T2> : IEcsRunSystem
    where T1 : struct, IWeaponComponent
    where T2 : BaseWeaponConfig, IWeaponConfig<T1>
{
    protected EcsWorldInject _world = default;
    protected EcsCustomInject<WeaponConfig> _weaponConfig = default;
    protected EcsFilterInject<Inc<UpgradeWeaponComponent>> _upgradeWeaponFilter = default;
    protected EcsPoolInject<EntityWeaponsComponent> _entityWeaponsPool = default;
    protected EcsPoolInject<T1> _weaponPool = default;
    public void Run(EcsSystems systems)
    {
        var config = _weaponConfig.Value.GetWeaponConfig<T2>();
        foreach (var entity in _upgradeWeaponFilter.Value)
        {
            ref var upgradeWeapon = ref _upgradeWeaponFilter.Pools.Inc1.Get(entity);
            if (upgradeWeapon.WeaponType != config.Type) return;
            if (upgradeWeapon.Entity.Unpack(_world.Value, out var targetEntity) == false)
            {
                _world.Value.DelEntity(entity);
                continue;
            }

            ref var entityWeapons = ref _entityWeaponsPool.Value.Get(targetEntity);
            
            if (entityWeapons.Weapons.ContainsKey(upgradeWeapon.WeaponType))
            {
                var weaponLevel = entityWeapons.Weapons[upgradeWeapon.WeaponType];
                ref var weaponComponent = ref _weaponPool.Value.Get(targetEntity);
                
                LevelUp(ref weaponComponent, config, weaponLevel.Level);
                
                weaponLevel.Level++;
                entityWeapons.Weapons[upgradeWeapon.WeaponType] = weaponLevel;
            }
            else
            {
                ref var weaponComponent = ref _weaponPool.Value.Add(targetEntity);
                Initialize(ref weaponComponent, config);

                entityWeapons.Weapons[upgradeWeapon.WeaponType] = new WeaponLevel()
                {
                    Level = 1,
                    MaxLevel = config.MaxLevel
                };
            }
            _world.Value.DelEntity(entity);
        }
    }

    protected abstract void LevelUp(ref T1 commonWeapon, T2 config, int weaponLevel);
    protected abstract void Initialize(ref T1 commonWeapon, T2 config);
}