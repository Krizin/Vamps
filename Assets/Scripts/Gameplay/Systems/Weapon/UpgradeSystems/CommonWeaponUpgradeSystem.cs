﻿public class CommonWeaponUpgradeSystem : UpgradeWeaponSystem<CommonWeaponComponent, CommonWeaponConfig>
{
    protected override void LevelUp(ref CommonWeaponComponent commonWeapon, CommonWeaponConfig config, int weaponLevel)
    {
        commonWeapon.Damage += config.CommonWeaponLevelUps[weaponLevel].Damage;
        commonWeapon.Reload += config.CommonWeaponLevelUps[weaponLevel].Reload;
        commonWeapon.ProjectileHitLimit += config.CommonWeaponLevelUps[weaponLevel].ProjectileHitLimit;
    }

    protected override void Initialize(ref CommonWeaponComponent commonWeapon, CommonWeaponConfig config)
    {
        commonWeapon.Damage = config.BaseDamage;
        commonWeapon.Speed = config.BaseSpeed;
        commonWeapon.Reload = config.BaseReload;
        commonWeapon.TriggerRadius = config.TriggerRadius;
        commonWeapon.ProjectilePrefab = config.ProjectilePrefab;
        commonWeapon.ProjectileHitLimit = config.ProjectileHitLimit;
        commonWeapon.ProjectileLifetimeLimit = config.ProjectileLifetimeLimit;
    }
}