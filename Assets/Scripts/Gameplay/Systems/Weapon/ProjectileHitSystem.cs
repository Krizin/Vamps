﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Нанесение урона при прпадании снаряда
/// </summary>
public class ProjectileHitSystem : IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsCustomInject<GlobalWeaponConfig> _globalWeaponSettings;
    private readonly EcsFilterInject<Inc<InteractionComponent>> _interactionFilter = default;
    private readonly EcsPoolInject<InteractionComponent> _interactionPool = default;
    private readonly EcsPoolInject<HealthComponent> _healthPool = default;
    private readonly EcsPoolInject<ProjectileComponent> _projectilePool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _interactionFilter.Value)
        {
            ref var interactionComponent = ref _interactionPool.Value.Get(entity);
            if (interactionComponent.OtherType != EntityType.Projectile) continue;
            
            if (interactionComponent.OtherEntity.Unpack(_world.Value, out var otherEntity) == false ||
                interactionComponent.OwnerEntity.Unpack(_world.Value, out var ownerEntity) == false)
            {
                _world.Value.DelEntity(entity);
                continue;
            }
            if (!_healthPool.Value.Has(ownerEntity)) continue;

            ref var health = ref _healthPool.Value.Get(ownerEntity);
            ref var projectile = ref _projectilePool.Value.Get(otherEntity);
            if (projectile.LastHitTime + _globalWeaponSettings.Value.ProjectileDamageFrequancy > Time.time) continue;
            projectile.LastHitTime = Time.time;
            health.CurrentHealth -= projectile.Damage;
            projectile.HitCount++;
        }
    }
}