﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Выстрел первого проджектайла оружия, создающего наносящие урон области
/// </summary>
public class WaterWeaponFirstAttackSystem : IEcsInitSystem, IEcsRunSystem
{
    private readonly EcsCustomInject<GlobalWeaponConfig> _globalWeaponSettings;
    private Transform _playerTransform;
    private Transform _projectileParent;
    public void Init(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var playerFilter = ecsWorld.Filter<PlayerComponent>().End();
        if (playerFilter.GetEntitiesCount() == 0) return;
        var transformPool = ecsWorld.GetPool<TransformComponent>();
         var playerEntity = playerFilter.GetRawEntities()[0];
        _playerTransform = transformPool.Get(playerEntity).Transform;
        var projectileParentEntity = ecsWorld.Filter<ProjectileParentComponent>().Inc<TransformComponent>().End().GetRawEntities()[0];
        _projectileParent = transformPool.Get(projectileParentEntity).Transform;
    }
    public void Run(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();

        if (!_playerTransform || !_projectileParent) return;
        
        var waterWeaponFilter = ecsWorld.Filter<WaterWeaponComponent>().End();
        var waterWeaponPool = ecsWorld.GetPool<WaterWeaponComponent>();
        var waterProjectilePool = ecsWorld.GetPool<WaterProjectileComponent>();
        var transformPool = ecsWorld.GetPool<TransformComponent>();
        
        foreach (var waterWeaponEntity in waterWeaponFilter)
        {
            ref var waterWeapon = ref waterWeaponPool.Get(waterWeaponEntity);
            
            if (waterWeapon.LastShotTime + waterWeapon.Reload > Time.time) continue;

            waterWeapon.LastShotTime = Time.time;

            for (var i = 0; i < waterWeapon.Count; i++)
            {
                var waterProjectileEntity = ecsWorld.NewEntity();
                ref var waterProjectileComponent = ref waterProjectilePool.Add(waterProjectileEntity);
                ref var transform = ref transformPool.Add(waterProjectileEntity);

                var instance = Object.Instantiate(waterWeapon.FirstProjectilePrefab, _projectileParent);
                transform.Transform = instance.transform;
                transform.Transform.position = _playerTransform.position;
                
                var minRange = _globalWeaponSettings.Value.MinWaterWeaponRange;
                var maxRange = _globalWeaponSettings.Value.MaxWaterWeaponRange;
                var randomValue = Random.insideUnitCircle;
                var direction = randomValue * (maxRange - minRange) + randomValue.normalized * minRange;
                var gravity = Physics.gravity.y;
                var projectileFlightTime = waterWeapon.ProjectileFlightTime;
                var velocity = new Vector3(direction.x / projectileFlightTime, projectileFlightTime / 2 * -gravity, direction.y / projectileFlightTime);
                transform.Transform.GetComponent<Rigidbody>().AddForce(velocity, ForceMode.VelocityChange);

                waterProjectileComponent.Damage = waterWeapon.Damage;
                waterProjectileComponent.LifetimeLimit = waterWeapon.ProjectileLifetimeLimit;
                waterProjectileComponent.HitLimit = waterWeapon.HitLimit;
                waterProjectileComponent.SecondProjectilePrefab = waterWeapon.SecondProjectilePrefab;
                waterProjectileComponent.SizeMultiplier = waterWeapon.SizeMultiplier;
            }
        }
    }
}