﻿using Leopotam.EcsLite;
using UnityEngine;

/// <summary>
/// Оружие, стреляющее одиночными снарядами по ближайшим врагам
/// </summary>
public class CommonWeaponAttackSystem : IEcsInitSystem, IEcsRunSystem
{
    private EcsPackedEntity _projectileParentEntity;
    public void Init(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var projectileParentFilter = ecsWorld.Filter<ProjectileParentComponent>().Inc<TransformComponent>().End();
        if (projectileParentFilter.GetEntitiesCount() == 0) return;
        _projectileParentEntity = ecsWorld.PackEntity(projectileParentFilter.GetRawEntities()[0]);
    }
    public void Run(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();

        if (_projectileParentEntity.Unpack(ecsWorld, out var projectileParentEntity) == false) return;
        
        var nearestEnemyFilter = ecsWorld.Filter<NearestEnemyComponent>().Inc<TransformComponent>().End();
        var playerFilter = ecsWorld.Filter<PlayerComponent>().Inc<TransformComponent>().Inc<CommonWeaponComponent>().End();
        
        if (playerFilter.GetEntitiesCount() == 0 || nearestEnemyFilter.GetEntitiesCount() == 0) return;
        var playerEntity = playerFilter.GetRawEntities()[0];
        var enemyEntity = nearestEnemyFilter.GetRawEntities()[0];
        
        var commonWeaponPool = ecsWorld.GetPool<CommonWeaponComponent>();
        ref var commonWeapon = ref commonWeaponPool.Get(playerEntity);
        if (commonWeapon.LastShotTime + commonWeapon.Reload > Time.time) return;
        commonWeapon.LastShotTime = Time.time;

        var transformPool = ecsWorld.GetPool<TransformComponent>();
        var playerTransform = transformPool.Get(playerEntity);
        var enemyTransform = transformPool.Get(enemyEntity);

        var playerToEnemyVector = enemyTransform.Transform.position - playerTransform.Transform.position;
        if (playerToEnemyVector.magnitude > commonWeapon.TriggerRadius) return;
        
        var projectileEntity = ecsWorld.NewEntity();
        
        ref var projectileComponent = ref ecsWorld.GetPool<ProjectileComponent>().Add(projectileEntity);
        ref var directionMoveComponent = ref ecsWorld.GetPool<DirectionMoveComponent>().Add(projectileEntity);
        ref var rigidbodyComponent = ref ecsWorld.GetPool<RigidbodyComponent>().Add(projectileEntity);
        ref var projectileTransform = ref transformPool.Add(projectileEntity);
        ref var colliderComponent = ref ecsWorld.GetPool<ColliderComponent>().Add(projectileEntity);
        ecsWorld.GetPool<DestroyOutsideScreenComponent>().Add(projectileEntity);
        
        var projectileParent = transformPool.Get(projectileParentEntity);
        
        projectileComponent.Damage = commonWeapon.Damage;
        projectileComponent.HitLimit = commonWeapon.ProjectileHitLimit;
        projectileComponent.LifetimeLimit = commonWeapon.ProjectileLifetimeLimit;
        directionMoveComponent.Direction = playerToEnemyVector.normalized;
        directionMoveComponent.Speed = commonWeapon.Speed;

        var spawnPosition = playerTransform.Transform.position;
        var instance = Object.Instantiate(commonWeapon.ProjectilePrefab, spawnPosition, Quaternion.identity);
        
        colliderComponent.Collider = instance.GetComponentInChildren<Collider>();
        rigidbodyComponent.Rigidbody = instance.GetComponent<Rigidbody>();
        projectileTransform.Transform = instance.transform;
        projectileTransform.Transform.SetParent(projectileParent.Transform);

    }

}