﻿using Leopotam.EcsLite;
using UnityEngine;

/// <summary>
/// Спавн второго проджектайла оружия, создающего наносящие урон области
/// </summary>
public class WaterWeaponSecondAttackSystem : IEcsInitSystem, IEcsRunSystem
{
    private EcsPackedEntity _projectileParentEntity;
    public void Init(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var projectileParentFilter = ecsWorld.Filter<ProjectileParentComponent>().Inc<TransformComponent>().End();
        if (projectileParentFilter.GetEntitiesCount() == 0) return;
        _projectileParentEntity = ecsWorld.PackEntity(projectileParentFilter.GetRawEntities()[0]);
    }
    public void Run(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();

        if (_projectileParentEntity.Unpack(ecsWorld, out var projectileParentEntity) == false) return;
        var interactionFilter = ecsWorld.Filter<InteractionComponent>().End();
        var interactionPool = ecsWorld.GetPool<InteractionComponent>();
        var waterProjectilePool = ecsWorld.GetPool<WaterProjectileComponent>();
        var projectilePool = ecsWorld.GetPool<ProjectileComponent>();
        var transformPool = ecsWorld.GetPool<TransformComponent>();

        var projectileParent = transformPool.Get(projectileParentEntity);

        foreach (var interactionEntity in interactionFilter)
        {
            ref var interactionComponent = ref interactionPool.Get(interactionEntity);
            if (interactionComponent.OwnerType != EntityType.WaterProjectile ||
                interactionComponent.OtherType != EntityType.Ground) continue;
            if (interactionComponent.OwnerEntity.Unpack(ecsWorld, out var ownerEntity) == false)
            {
                ecsWorld.DelEntity(interactionEntity);
                continue;
            }

            var waterProjectile = waterProjectilePool.Get(ownerEntity);
            var waterProjectileTransform = transformPool.Get(ownerEntity);
            var waterProjectilePosition = transformPool.Get(ownerEntity).Transform.position;
            
            var newProjectileEntity = ecsWorld.NewEntity();
            ref var projectile = ref projectilePool.Add(newProjectileEntity);
            ref var projectileTransform = ref transformPool.Add(newProjectileEntity);

            var instance = Object.Instantiate(waterProjectile.SecondProjectilePrefab, projectileParent.Transform);
            projectileTransform.Transform = instance.transform;
            projectileTransform.Transform.position =
                new Vector3(waterProjectilePosition.x, 0, waterProjectilePosition.z);
            projectileTransform.Transform.localScale *= waterProjectile.SizeMultiplier;

            projectile.Damage = waterProjectile.Damage;
            projectile.LifetimeLimit = waterProjectile.LifetimeLimit;
            projectile.HitLimit = waterProjectile.HitLimit;
            
            Object.Destroy(waterProjectileTransform.Transform.gameObject);
            ecsWorld.DelEntity(ownerEntity);
            ecsWorld.DelEntity(interactionEntity);
        }
    }

}