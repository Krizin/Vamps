﻿using Leopotam.EcsLite;
using UnityEngine;

/// <summary>
/// Оружие, атакующее снарядами, вращающимися вокруг игрока
/// </summary>
public class BatWeaponAttackSystem : IEcsInitSystem, IEcsRunSystem
{
    private EcsPackedEntity _projectileParentEntity;
    public void Init(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var projectileParentFilter = ecsWorld.Filter<ProjectileParentComponent>().Inc<TransformComponent>().End();
        if (projectileParentFilter.GetEntitiesCount() == 0) return;
        _projectileParentEntity = ecsWorld.PackEntity(projectileParentFilter.GetRawEntities()[0]);
    }
    public void Run(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();

        if (_projectileParentEntity.Unpack(ecsWorld, out var projectileParentEntity) == false) return;
        var playerFilter = ecsWorld.Filter<PlayerComponent>().Inc<TransformComponent>().Inc<BatWeaponComponent>().End();
        if (playerFilter.GetEntitiesCount() == 0) return;
        var playerEntity = playerFilter.GetRawEntities()[0];
        
        var batWeaponPool = ecsWorld.GetPool<BatWeaponComponent>();
        var projectilePool = ecsWorld.GetPool<ProjectileComponent>();
        var moveAroundPool = ecsWorld.GetPool<MoveAroundComponent>();
        var rigidbodyPool = ecsWorld.GetPool<RigidbodyComponent>();
        var transformPool = ecsWorld.GetPool<TransformComponent>();
        
        ref var batWeapon = ref batWeaponPool.Get(playerEntity);
        if (batWeapon.LastShotTime + batWeapon.Reload > Time.time) return;
        batWeapon.LastShotTime = Time.time;
        
        var playerTransform = transformPool.Get(playerEntity);
        var projectileParent = transformPool.Get(projectileParentEntity);

        var offset = Vector3.right * batWeapon.Radius;
        for (var i = 0; i < batWeapon.ProjectilesCount; i++)
        {
            var projectileEntity = ecsWorld.NewEntity();
            
            ref var projectileComponent = ref projectilePool.Add(projectileEntity);
            ref var moveAroundComponent = ref moveAroundPool.Add(projectileEntity);
            ref var rigidbodyComponent = ref rigidbodyPool.Add(projectileEntity);
            ref var projectileTransform = ref transformPool.Add(projectileEntity);
            
            projectileComponent.Damage = batWeapon.Damage;
            projectileComponent.HitLimit = batWeapon.ProjectileHitLimit;
            projectileComponent.LifetimeLimit = batWeapon.ProjectileLifetimeLimit;

            moveAroundComponent.Speed = batWeapon.Speed;
            moveAroundComponent.Target = playerTransform.Transform;
            moveAroundComponent.CurrentOffset = offset;

            var spawnPosition = playerTransform.Transform.position + offset;
            var instance = Object.Instantiate(batWeapon.ProjectilePrefab, spawnPosition, Quaternion.identity);
            
            rigidbodyComponent.Rigidbody = instance.GetComponent<Rigidbody>();
            projectileTransform.Transform = instance.transform;
            projectileTransform.Transform.SetParent(projectileParent.Transform);

            offset = Quaternion.Euler(0, 360f / batWeapon.ProjectilesCount, 0) * offset;
            
        }
    }
}