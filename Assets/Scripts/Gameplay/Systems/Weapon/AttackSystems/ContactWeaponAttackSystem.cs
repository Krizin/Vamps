﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Оружие, наносящее контактный урон
/// </summary>

// TODO: Реализовать через спавн преследующего снаряда
public class ContactWeaponAttackSystem : IEcsRunSystem
{
    private readonly EcsCustomInject<GlobalWeaponConfig> _globalWeaponSettings;
    public void Run(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var interactionFilter = ecsWorld.Filter<InteractionComponent>().End();
        var interactionPool = ecsWorld.GetPool<InteractionComponent>();
        var contactWeaponPool = ecsWorld.GetPool<ContactWeaponComponent>();
        var healthPool = ecsWorld.GetPool<HealthComponent>();

        foreach (var entity in interactionFilter)
        {
            var interactionComponent = interactionPool.Get(entity);
            if (interactionComponent.OtherEntity.Unpack(ecsWorld, out var otherEntity) == false ||
                interactionComponent.OwnerEntity.Unpack(ecsWorld, out var ownerEntity) == false)
            {
                ecsWorld.DelEntity(entity);
                continue;
            }
            if (!(contactWeaponPool.Has(ownerEntity) &&
                  healthPool.Has(otherEntity)))
                continue;
            ref var contactWeapon = ref contactWeaponPool.Get(ownerEntity);
            ref var health = ref healthPool.Get(otherEntity);
            if (contactWeapon.LastHitTime + _globalWeaponSettings.Value.ContactDamageFrequancy > Time.time) continue;
            health.CurrentHealth -= contactWeapon.Damage;
        }
        foreach (var entity in interactionFilter)
        {
            var interactionComponent = interactionPool.Get(entity);
            if (interactionComponent.OtherEntity.Unpack(ecsWorld, out var otherEntity) == false ||
                interactionComponent.OwnerEntity.Unpack(ecsWorld, out var ownerEntity) == false)
            {
                ecsWorld.DelEntity(entity);
                continue;
            }
            if (!(contactWeaponPool.Has(ownerEntity) &&
                  healthPool.Has(otherEntity)))
                continue;
            ref var contactWeapon = ref contactWeaponPool.Get(ownerEntity);
            if (contactWeapon.LastHitTime + _globalWeaponSettings.Value.ContactDamageFrequancy > Time.time) continue;
            contactWeapon.LastHitTime = Time.time;
        }
    }
}