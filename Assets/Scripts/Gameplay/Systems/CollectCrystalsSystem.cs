﻿using Gamebase;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Подбор кристаллов опыта игроком
/// </summary>
public class CollectCrystalsSystem : IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsCustomInject<ProgressSystem> _progressSystem;
    private readonly EcsFilterInject<Inc<InteractionComponent>> _interactionFilter = default;
    private readonly EcsPoolInject<InteractionComponent> _interactionPool = default;
    private readonly EcsPoolInject<CrystalComponent> _crystalPool = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _interactionFilter.Value)
        {
            ref var interactionComponent = ref _interactionPool.Value.Get(entity);
            if (interactionComponent.OwnerType != EntityType.Player ||
                interactionComponent.OtherType != EntityType.Crystal)
                continue;
            if (interactionComponent.OtherEntity.Unpack(_world.Value, out var otherEntity) == false)
            {
                _world.Value.DelEntity(entity);
                continue;
            }
            ref var crystal = ref _crystalPool.Value.Get(otherEntity);
            _progressSystem.Value.AddXP(crystal.Experience);
            Object.Destroy(_transformPool.Value.Get(otherEntity).Transform.gameObject);
            _world.Value.DelEntity(otherEntity);
            _world.Value.DelEntity(entity);
        }
    }
}