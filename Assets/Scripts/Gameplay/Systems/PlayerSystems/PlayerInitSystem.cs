﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Создание и инициализация игрока
/// </summary>
public class PlayerInitSystem : IEcsRunSystem
{
    private EcsWorldInject _world = default;
    private readonly EcsCustomInject<PlayerConfig> _playerConfig;
    private EcsFilterInject<Inc<PlayerInitComponent>> _playerInitFilter = default;
    private EcsPoolInject<DirectionMoveComponent> _directionMovePool = default;
    private EcsPoolInject<HealthComponent> _healthPool = default;
    private EcsPoolInject<UpgradeWeaponComponent> _upgradeWeaponPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _playerInitFilter.Value)
        {
            ref var healthComponent = ref _healthPool.Value.Get(entity);
            healthComponent.MaxHealth = _playerConfig.Value.Health;
            healthComponent.CurrentHealth = _playerConfig.Value.Health;
            
            ref var directionMoveComponent = ref _directionMovePool.Value.Get(entity);
            directionMoveComponent.Speed = _playerConfig.Value.Speed;
            
            foreach (var weaponType in _playerConfig.Value.BaseWeapons)
            {
                ref var upgradeWeaponComponent = ref _upgradeWeaponPool.Value.Add(_world.Value.NewEntity());
                upgradeWeaponComponent.Entity = _world.Value.PackEntity(entity);
                upgradeWeaponComponent.WeaponType = weaponType;
            }
            _playerInitFilter.Pools.Inc1.Del(entity);
        }
    }
}