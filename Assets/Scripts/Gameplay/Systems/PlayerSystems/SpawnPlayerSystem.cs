﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

public class SpawnPlayerSystem : IEcsInitSystem
{
    private readonly EcsCustomInject<PlayerConfig> _playerConfig;
    public void Init(EcsSystems systems)
    {
        Object.Instantiate(_playerConfig.Value.Prefab, Vector3.zero, Quaternion.identity);
    }
}