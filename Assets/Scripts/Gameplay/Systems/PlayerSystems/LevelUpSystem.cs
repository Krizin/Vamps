﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gamebase;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using Random = UnityEngine.Random;

/// <summary>
/// Определение выбора орудий для улучшения при повышении уровня
/// </summary>
public class LevelUpSystem : IEcsRunSystem
{
    private readonly EcsFilterInject<Inc<PlayerComponent, EntityWeaponsComponent>> _playerFilter = default;
    private readonly EcsCustomInject<ProgressSystem> _progressSystem;
    private readonly EcsCustomInject<GameSettings> _gameSettings;
    private EcsPoolInject<LevelUpComponent> _levelUpPool = default;
    private EcsPoolInject<EntityWeaponsComponent> _entityWeaponsPool = default;
    private static ProgressSettings Settings => ProgressSettings.Instance;
    public void Run(EcsSystems systems)
    {
        var requiredXp =  Settings.levelSettings[_progressSystem.Value.CurrentProgressLevel].XpRequired;
        if (_progressSystem.Value.CurrentXP < requiredXp) return;
        ChooseWeaponsToUpgrade(systems);
        _progressSystem.Value.OpenNextLevel();
        _progressSystem.Value.AddXP(-requiredXp);
    }
    private void ChooseWeaponsToUpgrade(EcsSystems systems)
    {
        foreach (var playerEntity in _playerFilter.Value)
        {
            ref var playerWeaponsComponent = ref _entityWeaponsPool.Value.Get(playerEntity);
            var playerWeapons = playerWeaponsComponent.Weapons;

            var allUpgradableWeapons = Enum.GetValues(typeof(WeaponType)).Cast<WeaponType>().Where(weaponType =>
                !playerWeapons.ContainsKey(weaponType) ||
                playerWeapons[weaponType].Level < playerWeapons[weaponType].MaxLevel).ToList();

            var playerUpgradableWeapons = (from weaponLevel in playerWeapons
                where weaponLevel.Value.Level < weaponLevel.Value.MaxLevel
                select weaponLevel.Key).ToList();

            ref var levelUpComponent = ref _levelUpPool.Value.Add(playerEntity);
            levelUpComponent.Upgrades = new List<WeaponType>();
            var levelUpUpgradesCount = _gameSettings.Value.LevelUpUpgradesCount;
            if (playerUpgradableWeapons.Count != 0)
            {
                levelUpComponent.Upgrades.Add(playerUpgradableWeapons[Random.Range(0, playerUpgradableWeapons.Count)]);
                allUpgradableWeapons.Remove(levelUpComponent.Upgrades[0]);
                levelUpUpgradesCount--;
            }

            for (var i = 0; i < levelUpUpgradesCount && allUpgradableWeapons.Count > 0; i++)
            {
                var nextWeapon = allUpgradableWeapons[Random.Range(0, allUpgradableWeapons.Count)];
                levelUpComponent.Upgrades.Add(nextWeapon);
                allUpgradableWeapons.Remove(nextWeapon);
            } 
        }
    }
}