using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Чтение инпута пользователя
/// </summary>
public class PlayerInputSystem : IEcsRunSystem
{
    private EcsFilterInject<Inc<PlayerComponent, DirectionMoveComponent>> _playerFilter = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _playerFilter.Value)
        {
            ref var directionMoveComponent = ref _playerFilter.Pools.Inc2.Get(entity);
            directionMoveComponent.Direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        }
    }
}
