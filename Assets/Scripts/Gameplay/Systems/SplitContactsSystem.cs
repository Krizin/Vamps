﻿using System;
using Leopotam.EcsLite;
using UnityEngine;

/// <summary>
/// Определение участников столкновений текущего цикла (FixedUpdate)
/// </summary>
public class SplitContactsSystem : IEcsInitSystem, IEcsRunSystem
{
    private EcsPackedEntity _playerEntity;
    private Transform _playerTransform;
    public void Init(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var playerFilter = ecsWorld.Filter<PlayerComponent>().Inc<TransformComponent>().End();
        if (playerFilter.GetEntitiesCount() == 0) return;
        _playerEntity = ecsWorld.PackEntity(playerFilter.GetRawEntities()[0]);
        _playerTransform = ecsWorld.GetPool<TransformComponent>().Get(playerFilter.GetRawEntities()[0]).Transform;
    }
    public void Run(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var interactionFilter = ecsWorld.Filter<InteractionComponent>().End();
        var interactionPool = ecsWorld.GetPool<InteractionComponent>();
        var enemiesFilter = ecsWorld.Filter<EnemyComponent>().End();
        var crystalFilter = ecsWorld.Filter<CrystalComponent>().End();
        var projectileFilter = ecsWorld.Filter<ProjectileComponent>().End();
        var groundFilter = ecsWorld.Filter<GroundComponent>().End();
        var waterProjectileFilter = ecsWorld.Filter<WaterProjectileComponent>().End();
        var transformPool = ecsWorld.GetPool<TransformComponent>();

        var isPlayerExist = _playerEntity.Unpack(ecsWorld, out var playerEntity);
        
        
        foreach (var interactionEntity in interactionFilter)
        {
            ref var interactionComponent = ref interactionPool.Get(interactionEntity);

            var ownerValues = GetEntityByTransform(interactionComponent.Owner, EntityType.Player, EntityType.Enemy,
                EntityType.WaterProjectile);
            
            if (ownerValues.EntityType == EntityType.Unidentified)
            {
                ecsWorld.DelEntity(interactionEntity);
                continue;
            }
            
            interactionComponent.OwnerEntity = ecsWorld.PackEntity(ownerValues.Entity);
            interactionComponent.OwnerType = ownerValues.EntityType;
            var otherTypes = ownerValues.EntityType switch
            {
                EntityType.Player => new [] {EntityType.Enemy, EntityType.Crystal},
                EntityType.Enemy => new [] {EntityType.Player, EntityType.Projectile},
                EntityType.WaterProjectile => new [] {EntityType.Ground},
                _ => Array.Empty<EntityType>()
            };
            
            var otherValues =
                GetEntityByTransform(interactionComponent.Other, otherTypes);
            
            if (otherValues.EntityType == EntityType.Unidentified)
                ecsWorld.DelEntity(interactionEntity);
            else
            {
                interactionComponent.OtherEntity = ecsWorld.PackEntity(otherValues.Entity);
                interactionComponent.OtherType = otherValues.EntityType;
            }
        }

        // Поиск target среди существущих сущностей типов entityTypes
        (int Entity, EntityType EntityType) GetEntityByTransform(Transform target, params EntityType[] entityTypes)
        {
            foreach (var entityType in entityTypes)
            {
                switch (entityType)
                {
                    case EntityType.Unidentified:
                        continue;
                    case EntityType.Player:
                        if (isPlayerExist && _playerTransform == target)
                            return (playerEntity, EntityType.Player);
                        continue;
                }
                
                var filter = entityType switch
                {
                    EntityType.Enemy => enemiesFilter,
                    EntityType.Crystal => crystalFilter,
                    EntityType.Projectile => projectileFilter,
                    EntityType.Ground => groundFilter,
                    EntityType.WaterProjectile => waterProjectileFilter,
                    _ => throw new ArgumentOutOfRangeException()
                };
                foreach (var entity in filter)
                {
                    if (transformPool.Get(entity).Transform == target)
                        return (entity, entityType);
                }
            }

            return (-1, EntityType.Unidentified);
        }
    }

}