﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Уничтожает GameObject'ы за пределами камеры
/// </summary>
public class DestroyOutsideCameraSystem : IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsFilterInject<Inc<DestroyOutsideScreenComponent, ColliderComponent, TransformComponent>>
        _destroyOutsideCameraFilter = default;
    private readonly EcsFilterInject<Inc<MainCameraComponent, CameraComponent>> _cameraFilter = default;
    
    private readonly EcsPoolInject<ColliderComponent> _colliderPool = default;
    private readonly EcsPoolInject<CameraComponent> _cameraPool = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var cameraEntity in _cameraFilter.Value)
        {
            ref var camera = ref _cameraPool.Value.Get(cameraEntity);
            foreach (var entity in _destroyOutsideCameraFilter.Value)
            {
                ref var collider = ref _colliderPool.Value.Get(entity);
                if (CheckVisible(camera.Camera, collider.Collider)) continue;

                ref var transform = ref _transformPool.Value.Get(entity);
                Object.Destroy(transform.Transform.gameObject);
                _world.Value.DelEntity(entity);
            }
        }
    }
    private static bool CheckVisible (Camera camera, Collider collider)
    {
        var planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB (planes, collider.bounds);
    }
}