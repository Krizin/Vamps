﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Уничтожение проджектайлов
/// </summary>
public class DestroyProjectileSystem : IEcsRunSystem
{
    private EcsWorldInject _world = default;
    private EcsFilterInject<Inc<ProjectileComponent, TransformComponent>> _projectileFilter = default;
    private EcsPoolInject<TransformComponent> _transformPool = default;
    private EcsPoolInject<ProjectileComponent> _projectilePool = default;
    public void Run(EcsSystems systems)
    {
        
        foreach (var entity in _projectileFilter.Value)
        {
            ref var projectile = ref _projectilePool.Value.Get(entity);
            ref var transform = ref _transformPool.Value.Get(entity);
            projectile.Lifetime += Time.deltaTime;
            if (projectile.HitCount < projectile.HitLimit && projectile.Lifetime < projectile.LifetimeLimit) continue;
            
            // TODO: Реализовать через пул
            Object.Destroy(transform.Transform.gameObject);
            _world.Value.DelEntity(entity);
        }
    }
}