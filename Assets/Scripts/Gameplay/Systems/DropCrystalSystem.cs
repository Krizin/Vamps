﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Создание GameObject'ов для кристаллов опыта
/// </summary>
public class DropCrystalSystem : IEcsRunSystem
{
    private readonly EcsCustomInject<ExperienceConfig> _experienceConfig;
    private readonly EcsCustomInject<ObjectsParents> _objectsParents;
    private readonly EcsFilterInject<Inc<CrystalComponent>, Exc<TransformComponent>> _crystalFilter = default;
    private readonly EcsPoolInject<CrystalComponent> _crystalPool = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _crystalFilter.Value)
        {
            ref var transformComponent = ref _transformPool.Value.Add(entity);
            ref var crystalComponent = ref _crystalPool.Value.Get(entity);
            transformComponent.Transform = Object.Instantiate(_experienceConfig.Value.CrystalPerfab, _objectsParents.Value.CrystalParent).transform;
            transformComponent.Transform.position = crystalComponent.Position;
        }
    }
}