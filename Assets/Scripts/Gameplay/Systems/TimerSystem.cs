﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Счет времени со старта уровня
/// </summary>
public class TimerSystem : IEcsRunSystem
{
    private EcsFilterInject<Inc<TimerComponent>> _timerFilter = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _timerFilter.Value)
        {
            ref var timerComponent = ref _timerFilter.Pools.Inc1.Get(entity);
            timerComponent.PassedTime = Time.time - timerComponent.StartTime;
        }
    }
}