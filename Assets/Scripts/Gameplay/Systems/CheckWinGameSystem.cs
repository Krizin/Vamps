﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Проверка завершения игры по таймеру
/// </summary>
public class CheckWinGameSystem : IEcsRunSystem
{
    private EcsWorldInject _world = default;
    private EcsCustomInject<GameSettings> _gameSettings;
    private EcsFilterInject<Inc<GameTimerComponent, TimerComponent>> _timerFilter = default;
    private EcsPoolInject<TimerComponent> _timerPool = default;
    private EcsPoolInject<EndGameComponent> _endGamePool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var timerEntity in _timerFilter.Value)
        {
            ref var timerComponent = ref _timerPool.Value.Get(timerEntity);

            if (timerComponent.PassedTime < _gameSettings.Value.TimeToWin) return;

            ref var endGameComponent = ref _endGamePool.Value.Add(_world.Value.NewEntity());
            endGameComponent.GameResult = GameResult.Win;
        }
    }
}