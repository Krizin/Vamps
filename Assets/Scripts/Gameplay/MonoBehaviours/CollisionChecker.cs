﻿using UnityEngine;
using Voody.UniLeo.Lite;

public class CollisionChecker : MonoBehaviour
{
    private void OnCollisionStay(Collision collision)
    {
        CreateContact(collision.collider);
    }

    private void OnTriggerStay(Collider other)
    {
        CreateContact(other);
    }

    private void CreateContact(Collider collider)
    {
        var world = WorldHandler.GetMainWorld();
        var hit = world.NewEntity();
        ref var interactionComponent = ref world.GetPool<InteractionComponent>().Add(hit);

        interactionComponent.Owner = transform;
        interactionComponent.Other = collider.transform.parent ? collider.transform.parent : collider.transform;
    }
}