﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "Gameplay/LevelSpawnersConfig")]
public class LevelSpawnersConfig : ScriptableObject
{
    [FormerlySerializedAs("EnemySpawns")] [SerializeField] public List<EnemySpawnerData> EnemySpawners;

    [System.Serializable]
    public struct EnemySpawnerData
    {
        [SerializeField] public EnemyConfig EnemyConfig;
        [SerializeField, MinValue(0)] public int Count;
        [SerializeField, MinValue(0)] public float Spread;
        [SerializeField, MinValue(0)] public float StartSpawnTime;
        [SerializeField, MinValue(0)] public SpawnerLifetimeLimitationType LifetimeLimitationType;
        [SerializeField, MinValue(0)] public float EndSpawnTime;
        [SerializeField, MinValue(0)] public int TriggersNumber;
        [SerializeField, MinValue(0)] public float SpawnRate;
    }
}