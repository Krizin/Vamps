﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Weapons/WeaponPool")]
public class WeaponConfig : ScriptableObject
{
    [SerializeField] public List<BaseWeaponConfig> Weapons;

    public T GetWeaponConfig<T>() where T : BaseWeaponConfig
    {
        return Weapons.First(weapon => weapon is T) as T;
    }

    public BaseWeaponConfig GetBaseWeaponConfig(WeaponType weaponType)
    {
        return Weapons.First(weapon => weapon.Type == weaponType);
    }
}