﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Weapons/CommonWeapon")]
public class CommonWeaponConfig : BaseWeaponConfig, IWeaponConfig<CommonWeaponComponent>
{
    [SerializeField] public float BaseSpeed;
    [SerializeField] public float TriggerRadius;
    [SerializeField] public float ProjectileLifetimeLimit;
    [SerializeField] public int ProjectileHitLimit;
    [SerializeField] public GameObject ProjectilePrefab;
    [SerializeField] public List<CommonWeaponLevelUp> CommonWeaponLevelUps;

    [Serializable]
    public struct CommonWeaponLevelUp
    {
        public int Damage;
        public float Reload;
        public int ProjectileHitLimit;
        public string Description;
    }

    public override string GetDescription(int level)
    {
        return CommonWeaponLevelUps[level - 1].Description;
    }
}