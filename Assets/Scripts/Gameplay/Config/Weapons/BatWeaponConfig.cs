﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Weapons/BatWeapon")]
public class BatWeaponConfig : BaseWeaponConfig, IWeaponConfig<BatWeaponComponent>
{
    [SerializeField] public float BaseSpeed;
    [SerializeField] public float Radius;
    [SerializeField] public int ProjectilesCount;
    [SerializeField] public float ProjectileLifetimeLimit;
    [SerializeField] public int ProjectileHitLimit;
    [SerializeField] public GameObject ProjectilePrefab;
    [SerializeField] public List<BatWeaponLevelUp> BatWeaponLevelUps;

    [Serializable]
    public struct BatWeaponLevelUp
    {
        public int Damage;
        public float Reload;
        public float Radius;
        public float Speed;
        public int ProjectilesCount;
        public float ProjectileLifetimeLimit;
        public string Description;
    }

    public override string GetDescription(int level)
    {
        return BatWeaponLevelUps[level - 1].Description;
    }
}