﻿using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Weapons/GlobalWeaponSettings")]
public class GlobalWeaponConfig : ScriptableObject
{
    [SerializeField] public float ContactDamageFrequancy;
    [SerializeField] public float ProjectileDamageFrequancy;
    [SerializeField] public float MaxWaterWeaponRange;
    [SerializeField] public float MinWaterWeaponRange;
}