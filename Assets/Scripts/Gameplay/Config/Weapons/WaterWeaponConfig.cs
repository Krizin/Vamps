﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Weapons/WaterWeapon")]
public class WaterWeaponConfig : BaseWeaponConfig, IWeaponConfig<WaterWeaponComponent>
{
    [SerializeField] public float ProjectileLifetimeLimit;
    [SerializeField] public int HitLimit;
    [SerializeField] public float BaseSizeMultilpyer;
    [SerializeField] public int Count;
    [SerializeField] public float ProjectileFlightTime;
    [SerializeField] public GameObject FirstProjectilePrefab;
    [SerializeField] public GameObject SecondProjectilePrefab;
    [SerializeField] public List<WaterWeaponLevelUp> WaterWeaponLevelUps;
    [Serializable]
    public struct WaterWeaponLevelUp
    {
        public int Damage;
        public float Reload;
        public float BaseSizeMultilpier;
        public float ProjectileLifetimeLimit;
        public int Count;
        public string Description;
    }

    public override string GetDescription(int level)
    {
        return WaterWeaponLevelUps[level - 1].Description;
    }
}