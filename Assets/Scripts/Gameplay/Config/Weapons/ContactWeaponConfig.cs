﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Weapons/ContactWeapon")]
public class ContactWeaponConfig : BaseWeaponConfig, IWeaponConfig<ContactWeaponComponent>
{
    [SerializeField] public List<ContactLevelUpBoost> ContactLevelUps;
    
    [Serializable]
    public struct ContactLevelUpBoost
    {
        public int Damage;
        public string Description;
    }

    public override string GetDescription(int level)
    {
        return ContactLevelUps[level - 1].Description;
    }
}