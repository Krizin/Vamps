﻿using UnityEngine;

public abstract class BaseWeaponConfig : ScriptableObject
{
    [SerializeField] public string Name;
    [SerializeField] public Sprite Icon;
    [SerializeField] public WeaponType Type;
    [SerializeField] public int MaxLevel;
    [SerializeField] public int BaseDamage;
    [SerializeField] public float BaseReload;

    public abstract string GetDescription(int level);
}