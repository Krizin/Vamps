﻿using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/ObjectsParents")]
public class ObjectsParents : ScriptableObject
{
    [SerializeField] public Transform EnemiesParent;
    [SerializeField] public Transform HealthBarsParent;
    [SerializeField] public Transform CrystalParent;
}