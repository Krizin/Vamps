using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/PlayerConfig")]
public class PlayerConfig : ScriptableObject
{
    [SerializeField] public GameObject Prefab;
    [SerializeField] public int Health;
    [SerializeField] public float Speed;

    [SerializeField] public List<WeaponType> BaseWeapons;
}
