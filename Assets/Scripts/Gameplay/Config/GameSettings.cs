﻿using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/GameSettings")]
public class GameSettings : ScriptableObject
{
    [SerializeField] public float TimeToWin;
    [SerializeField] public int LevelUpUpgradesCount;

    [Header("Camera Settings")]
    [SerializeField] public Vector3 CameraOffset;
    [SerializeField] public float CameraSmoothness;
    
    
    [Header("Gameplay Settings")]
    [SerializeField] public float SpawnRadius;
    
}