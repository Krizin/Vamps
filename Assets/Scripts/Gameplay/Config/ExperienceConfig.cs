﻿using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/ExperienceConfig")]
public class ExperienceConfig : ScriptableObject
{
    [SerializeField] public GameObject CrystalPerfab;
}