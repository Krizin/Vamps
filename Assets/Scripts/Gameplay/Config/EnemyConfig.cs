﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/EnemyConfig")]
public class EnemyConfig : ScriptableObject
{
    [SerializeField] public GameObject Prefab;
    [SerializeField] public int Damage;
    [SerializeField] public float Speed;
    [SerializeField] public int Health;
    [SerializeField] public int Experience;
    
    [SerializeField] public List<WeaponType> BaseWeapons;
}