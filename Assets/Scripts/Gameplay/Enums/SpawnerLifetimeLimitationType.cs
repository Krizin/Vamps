﻿public enum SpawnerLifetimeLimitationType
{
    ByEndTime,
    ByTriggersNumber
}