﻿public enum EntityType
{
    Unidentified,
    Player,
    Enemy,
    Crystal,
    Projectile,
    Ground,
    WaterProjectile
}