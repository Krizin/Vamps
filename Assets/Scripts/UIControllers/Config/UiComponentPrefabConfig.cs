﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Gameplay/UiComponentPrefabConfig")]
public class UiComponentPrefabConfig : ScriptableObject
{
    [SerializeField] public Slider Healthbar;
}