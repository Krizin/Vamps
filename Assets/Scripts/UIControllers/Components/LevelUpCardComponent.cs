﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct LevelUpCardComponent
{
    public GameObject GameObject;
    public Button Button;
    public Image Icon;
    public TMP_Text Name;
    public TMP_Text Level;
    public TMP_Text Description;
}