﻿using System;
using TMPro;

[Serializable]
public struct LevelUiComponent
{
    public TMP_Text Level;
}