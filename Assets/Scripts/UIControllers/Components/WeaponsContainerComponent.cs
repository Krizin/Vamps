﻿using System;
using UnityEngine;

[Serializable]
public struct WeaponsContainerComponent
{
    [HideInInspector] public int WeaponCount;
}