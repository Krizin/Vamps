﻿using System;
using TMPro;

[Serializable]
public struct KillsCounterUiComponent
{
    public TMP_Text KillsCounter;
}