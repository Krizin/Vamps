﻿using System;
using TMPro;

[Serializable]
public struct TMPTextComponent
{
    public TMP_Text Text;
}