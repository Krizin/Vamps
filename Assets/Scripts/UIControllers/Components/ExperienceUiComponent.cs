﻿using System;
using UnityEngine.UI;

[Serializable]
public struct ExperienceUiComponent
{
    public Slider ProgressBar;
}