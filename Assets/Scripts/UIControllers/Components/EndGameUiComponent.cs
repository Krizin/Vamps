﻿using System;
using TMPro;

[Serializable]
public struct EndGameUiComponent
{
    public TMP_Text Result;
}