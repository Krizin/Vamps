﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Вывод счетчика убийств
/// </summary>
public class KillsCounterUiSystem : IEcsRunSystem
{
    private EcsFilterInject<Inc<KillsCounterComponent>> _killsCounterFilter = default;
    private EcsFilterInject<Inc<KillsCounterUiComponent>> _killsCounterUiFilter = default;
    public void Run(EcsSystems systems)
    {
        foreach (var killsCounterEntity in _killsCounterFilter.Value)
        {
            ref var killsCounter = ref _killsCounterFilter.Pools.Inc1.Get(killsCounterEntity);
            foreach (var killsCounterUiEntity in _killsCounterUiFilter.Value)
            {
                ref var killsCounterUi = ref _killsCounterUiFilter.Pools.Inc1.Get(killsCounterUiEntity);
                killsCounterUi.KillsCounter.text = killsCounter.Kills.ToString();
            }
        }
    }
}