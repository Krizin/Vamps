﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Добавляет полосу здоровья при наличии соответствующего компонента
/// </summary>
public class CreateHealthbarSystem : IEcsRunSystem
{
    private readonly EcsCustomInject<UiComponentPrefabConfig> _uiComponentPrefabs;
    private readonly EcsCustomInject<ObjectsParents> _objectsParents;
    private EcsFilterInject<Inc<HealthBarComponent>> _healthBarFilter = default;
    public void Run(EcsSystems systems)
    {
        foreach (var entity in _healthBarFilter.Value)
        {
            ref var healthBar = ref _healthBarFilter.Pools.Inc1.Get(entity);
            if (!healthBar.Slider)
            {
                healthBar.Slider = Object.Instantiate(_uiComponentPrefabs.Value.Healthbar, _objectsParents.Value.HealthBarsParent);
            }
        }
    }

}