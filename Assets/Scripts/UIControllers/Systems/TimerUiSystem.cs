﻿using System;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Вывод таймера
/// </summary>
public class TimerUiSystem : IEcsRunSystem
{
    private EcsFilterInject<Inc<TimerComponent, TimerUiComponent, TMPTextComponent>> _timerFilter = default;
    private EcsPoolInject<TMPTextComponent> _tmpTextPool = default;
    private EcsPoolInject<TimerComponent> _timerPool = default;
    public void Run(EcsSystems systems)
    {
        foreach (var timerEntity in _timerFilter.Value)
        {
            ref var timerComponent = ref _timerPool.Value.Get(timerEntity);
            ref var textComponent = ref _tmpTextPool.Value.Get(timerEntity);
            textComponent.Text.text = TimeSpan.FromSeconds(timerComponent.PassedTime).ToString(@"mm\:ss");
        }
    }
}