﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Добавляет иконку оружия, полученного игроком
/// </summary>
public class AddWeaponIconSystem : IEcsInitSystem, IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsCustomInject<PlayerConfig> _playerConfig;
    private readonly EcsCustomInject<WeaponConfig> _weaponConfig;
    private readonly EcsFilterInject<Inc<AddWeaponIconComponent>> _addWeaponIconFilter = default;
    private readonly EcsFilterInject<Inc<WeaponsContainerComponent, TransformComponent>> _weaponContainerFilter = default;
    private readonly EcsPoolInject<AddWeaponIconComponent> _addWeaponIconPool = default;
    private readonly EcsPoolInject<WeaponsContainerComponent> _weaponContainerPool = default;
    private readonly EcsPoolInject<TransformComponent> _transformPool = default;

    public void Init(EcsSystems systems)
    {
        foreach (var weaponType in _playerConfig.Value.BaseWeapons)
        {
            var entity = _world.Value.NewEntity();
            ref var addWeaponIconComponent = ref _addWeaponIconPool.Value.Add(entity);
            addWeaponIconComponent.WeaponType = weaponType;
        }
    }
    public void Run(EcsSystems systems)
    {
        foreach (var containerEntity in _weaponContainerFilter.Value)
        {
            ref var weaponContainerComponent = ref _weaponContainerPool.Value.Get(containerEntity);
            ref var weaponContainerTransform = ref _transformPool.Value.Get(containerEntity);

            foreach (var entity in _addWeaponIconFilter.Value)
            {
                ref var addWeaponIconComponent = ref _addWeaponIconPool.Value.Get(entity);
                var imageTransform = weaponContainerTransform.Transform.GetChild(weaponContainerComponent.WeaponCount)
                    .GetChild(1);
                weaponContainerComponent.WeaponCount++;
                var spriteRenderer = imageTransform.GetComponent<Image>();
                var icon = _weaponConfig.Value.GetBaseWeaponConfig(addWeaponIconComponent.WeaponType).Icon;
                spriteRenderer.color = Color.white;
                spriteRenderer.sprite = icon;

                _world.Value.DelEntity(entity);
            }
        }
    }

}