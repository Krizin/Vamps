﻿using System;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

/// <summary>
/// Установка значений панели выбора улучшений при повышении уровня
/// </summary>
public class InitLevelUpPanelSystem : IEcsInitSystem, IEcsRunSystem
{
    private readonly EcsCustomInject<WeaponConfig> _weaponConfig;
    private EcsPackedEntity _playerEntity;

    public void Init(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        var playerFilter = ecsWorld.Filter<PlayerComponent>().End();
        if (playerFilter.GetEntitiesCount() == 0) return;
        _playerEntity = ecsWorld.PackEntity(playerFilter.GetRawEntities()[0]);
    }
    public void Run(EcsSystems systems)
    {
        var ecsWorld = systems.GetWorld();
        if (_playerEntity.Unpack(ecsWorld, out var playerEntity) == false) return;
        var levelUpFilter = ecsWorld.Filter<LevelUpComponent>().End();
        if (levelUpFilter.GetEntitiesCount() == 0) return;
        var levelUpEntity = levelUpFilter.GetRawEntities()[0];
        var levelUpComponent = ecsWorld.GetPool<LevelUpComponent>().Get(levelUpEntity);
        var levelUpCardFilter = ecsWorld.Filter<LevelUpCardComponent>().End();
        var levelUpCardPool = ecsWorld.GetPool<LevelUpCardComponent>();
        var entityWeaponsPool = ecsWorld.GetPool<EntityWeaponsComponent>();

        var playerWeapons = entityWeaponsPool.Get(playerEntity);
        var cardNumber = 0;
        foreach (var levelUpCardEntity in levelUpCardFilter)
        {
            ref var levelUpCard = ref levelUpCardPool.Get(levelUpCardEntity);
            if (levelUpComponent.Upgrades.Count <= cardNumber)
            {
                levelUpCard.GameObject.SetActive(false);
                continue;
            }
            levelUpCard.GameObject.SetActive(true);
            var weaponType = levelUpComponent.Upgrades[cardNumber];
            var weaponConfig = _weaponConfig.Value.GetBaseWeaponConfig(weaponType);
            levelUpCard.Icon.sprite = weaponConfig.Icon;
            levelUpCard.Icon.color = Color.white;
            levelUpCard.Name.text = weaponConfig.Name;

            var weaponLevel = playerWeapons.Weapons.ContainsKey(weaponType) ?
                playerWeapons.Weapons[weaponType].Level + 1 : 1;

            levelUpCard.Level.text = weaponLevel > 1 ? weaponLevel.ToString("Lv 0") : "Новое!";
            levelUpCard.Description.text = weaponConfig.GetDescription(weaponLevel);
            levelUpCard.Button.onClick.RemoveAllListeners();
            levelUpCard.Button.onClick.AddListener(() => CreateUpgradeWeaponEntity(ecsWorld, weaponType));
            if (weaponLevel == 1)
                levelUpCard.Button.onClick.AddListener(() => CreateAddWeaponIconEntity(ecsWorld, weaponType));
            cardNumber++;
        }
        ecsWorld.DelEntity(levelUpEntity);
    }

    private void CreateUpgradeWeaponEntity(EcsWorld ecsWorld, WeaponType weaponType)
    {
        var entity = ecsWorld.NewEntity();
        if (_playerEntity.Unpack(ecsWorld, out var playerEntity) == false) return;
        var upgradeWeaponPool = ecsWorld.GetPool<UpgradeWeaponComponent>();
        ref var upgradeWeaponComponent = ref upgradeWeaponPool.Add(entity);
        upgradeWeaponComponent.WeaponType = weaponType;
        upgradeWeaponComponent.Entity = ecsWorld.PackEntity(playerEntity);
    }

    private static void CreateAddWeaponIconEntity(EcsWorld ecsWorld, WeaponType weaponType)
    {
        var entity = ecsWorld.NewEntity();
        var addWeaponIconPool = ecsWorld.GetPool<AddWeaponIconComponent>();
        ref var addWeaponIconComponent = ref addWeaponIconPool.Add(entity);
        addWeaponIconComponent.WeaponType = weaponType;
    }
}