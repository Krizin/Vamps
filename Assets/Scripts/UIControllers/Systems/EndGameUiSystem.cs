﻿using Gamebase;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

/// <summary>
/// Завершение игры
/// </summary>
public class EndGameUiSystem : IEcsRunSystem
{
    private readonly EcsWorldInject _world = default;
    private readonly EcsCustomInject<GlobalEventsSystem> _globalEventSystem;
    private readonly EcsFilterInject<Inc<EndGameComponent>> _endGameFilter = default;
    private readonly EcsFilterInject<Inc<EndGameUiComponent>> _endGameUiFilter = default;
    public void Run(EcsSystems systems)
    {
        foreach (var endGameEntity in _endGameFilter.Value)
        {
            ref var endGame = ref _endGameFilter.Pools.Inc1.Get(endGameEntity);
            foreach (var endGameUiEntity in _endGameUiFilter.Value)
            {
                var endGameUi = _endGameUiFilter.Pools.Inc1.Get(endGameUiEntity);
                endGameUi.Result.text = endGame.GameResult == GameResult.Win ? "VICTORY!" : "DEFEAT!";
            }

            _world.Value.DelEntity(endGameEntity);
            _globalEventSystem.Value.Invoke(GlobalEventType.GameOver);
        }
        
    }
}