﻿    using Leopotam.EcsLite;
    using Leopotam.EcsLite.Di;
    using UnityEngine;

    /// <summary>
    /// Обновление значений и перемещение полосы здоровья
    /// </summary>
    public class HealthbarSystem : IEcsRunSystem
    {
        private EcsFilterInject<Inc<HealthBarComponent, HealthComponent, TransformComponent>> _healthBarFilter = default;
        private EcsPoolInject<HealthBarComponent> _healthBarPool = default;
        private EcsPoolInject<HealthComponent> _healthPool = default;
        private EcsPoolInject<TransformComponent> _transformPool = default;
        public void Run(EcsSystems systems)
        {
            foreach (var entity in _healthBarFilter.Value)
            {
                ref var healthBarComponent = ref _healthBarPool.Value.Get(entity);
                ref var healthComponent = ref _healthPool.Value.Get(entity);
                healthBarComponent.Slider.maxValue = healthComponent.MaxHealth;
                healthBarComponent.Slider.value = healthComponent.CurrentHealth;

                ref var transformComponent = ref _transformPool.Value.Get(entity);

                var translation = transformComponent.Transform.position + healthBarComponent.Offset -
                                  healthBarComponent.Slider.transform.position;
                healthBarComponent.Slider.transform.Translate(translation, Space.World);
            }
        }
    }