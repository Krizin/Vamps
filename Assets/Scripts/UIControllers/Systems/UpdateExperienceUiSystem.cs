﻿using Gamebase;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// Обновление опыта и повышение уровня
/// </summary>
public class UpdateExperienceUiSystem : IEcsInitSystem, IEcsDestroySystem
{
    private readonly EcsCustomInject<ProgressSystem> _progressSystem;
    private readonly EcsFilterInject<Inc<ExperienceUiComponent, SliderComponent>> _experienceUiFilter = default;
    private readonly EcsFilterInject<Inc<LevelUiComponent, TMPTextComponent>> _levelUiFilter = default;
    private readonly EcsPoolInject<TMPTextComponent> _tmpTextPool = default;
    private readonly EcsPoolInject<SliderComponent> _sliderPool = default;
    private static ProgressSettings Settings => ProgressSettings.Instance;
    public void Init(EcsSystems systems)
    {
        foreach (var experienceUiEntity in _experienceUiFilter.Value)
        {
            ref var progressBar = ref _sliderPool.Value.Get(experienceUiEntity);
            progressBar.Slider.value = 0;
            progressBar.Slider.maxValue = Settings.levelSettings[0].XpRequired;
        }
        foreach (var levelUiEntity in _levelUiFilter.Value)
        {
            ref var level = ref _tmpTextPool.Value.Get(levelUiEntity);
            level.Text.text = (_progressSystem.Value.CurrentProgressLevel + 1).ToString("LV 0");
        }
        
        _progressSystem.Value.OnXPChanged += OnXPChanged;
        _progressSystem.Value.OnProgressLevelChanged += OnLevelChanged;
    }

    public void Destroy(EcsSystems systems)
    {
        _progressSystem.Value.OnXPChanged -= OnXPChanged;
        _progressSystem.Value.OnProgressLevelChanged -= OnLevelChanged;
    }

    private void OnXPChanged(int experience)
    {
        foreach (var entity in _experienceUiFilter.Value)
        {
            _experienceUiFilter.Pools.Inc1.Get(entity).ProgressBar.value = experience;
        }
    }

    private void OnLevelChanged(int level)
    {
        foreach (var experienceUiEntity in _experienceUiFilter.Value)
        {
            _sliderPool.Value.Get(experienceUiEntity).Slider.maxValue = Settings.levelSettings[level].XpRequired;
        }
        foreach (var levelUiEntity in _levelUiFilter.Value)
        {
            _tmpTextPool.Value.Get(levelUiEntity).Text.text = (level + 1).ToString("LV 0");
        }
    }
}